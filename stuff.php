<?php
	require_once('_functions.php');

	$page = 'stuff';
	require_once('tpl/header.tpl');

	if ($_SERVER['REQUEST_METHOD'] === 'POST') {
		$params[0] = $_POST['stuff-shop']     ?? "";
		$params[1] = $_POST['stuff-name']     ?? "";
		$params[2] = $_POST['stuff-position'] ?? "";
		$params[3] = $_POST['stuff-year']     ?? "";
		$params[4] = $_POST['stuff-address']  ?? "";
		$params[5] = $_POST['stuff-phone']    ?? "";
		
		if (check_stuff_fields($params))
		{
			db_add_stuff($params);
		}
	}

	$table_data = db_get_stuff();

	$shops = db_get_shops(true);

?>
	<div class="container">
		<h2>Добавление персонала</h2>
		<form action="stuff.php" method="POST" id="form-add-stuff">
			<div class="row">
				<div class="col-md-4">
					<div class="form-group">
						<label class="control-label">Название магазина *</label>
						<select class="form-control" id="stuff-shop" name="stuff-shop">
							<?php
								foreach ($shops as $shop) {
									echo '<option value="' . $shop['id'] . '">' . $shop['name'] . '</option>';
								}
							?>
						</select>
					</div>
					<div class="form-group">
						<label class="control-label" for="stuff-name">Фамилия *</label>
						<input type="text" class="form-control" id="stuff-name" name="stuff-name" placeholder="Фамилия">
					</div>
					<div class="form-group">
						<label class="control-label" for="stuff-position">Должность *</label>
						<input type="text" class="form-control" id="stuff-position" name="stuff-position" placeholder="Должность">
					</div>
				</div>
				<div class="col-md-4">
					<div class="form-group">
						<label class="control-label" for="stuff-year">Год рождения *</label>
						<input type="text" class="form-control" id="stuff-year" name="stuff-year" placeholder="Год рождения">
					</div>
					<div class="form-group">
						<label class="control-label" for="stuff-address">Домашний адрес</label>
						<input type="text" class="form-control" id="stuff-address" name="stuff-address" placeholder="Домашний адрес">
					</div>
					<div class="form-group">
						<label class="control-label" for="stuff-phone">Телефон *</label>
						<input type="text" class="form-control" id="stuff-phone" name="stuff-phone" placeholder="+375XXXXXXXXX">
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-8">
					<button type="submit" class="btn btn-primary" id="add-stuff">Добавить</button>
					<button type="reset" class="btn btn-default">Очистить все поля</button>
				</div>
			</div>
		</form>


		<div class="row">
			<div class="col-md-12">
				<h2>Список персонала</h2>
				<?php render_table($title_stuff, $table_data); ?>
			</div>
		</div>
	</div>
<?php require_once('tpl/footer.tpl'); ?>