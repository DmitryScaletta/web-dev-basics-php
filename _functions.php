<?php

define('DATABASE_FILENAME', 'database.sqlite');
define('PHONE_REGEX',       '/^\+[\d]{12}$/');


/* ============================================================ */
/* checking */
/* ============================================================ */

function check_string($s, $not_compulsory = false) {
	if ($not_compulsory) return true;
	if (strlen($s) < 1) return false;
	return true;
}

function check_phone($s) {
	if (!preg_match(PHONE_REGEX, $s)) return false;
	return true;
}

function check_year($s) {
	if ($s < 1900 || $s > date('Y')) return false;
	return true;
}

function check_number(&$s) {
	$s = intval($s);
	if ($s < 1) return false;
	return true;
}

function check_shop_fields($name, $adress, $director_name, $phone) {
	if (check_string($name) &&
		check_string($adress) &&
		check_string($director_name, true) &&
		check_phone($phone)) return true;
	return false;
}

function check_stuff_fields($params) {
	if (check_number($params[0]) &&				// shop
		check_string($params[1]) &&				// name
		check_string($params[2]) &&				// position
		check_year(  $params[3]) &&				// year
		check_string($params[4], true) &&		// address
		check_phone( $params[5])) return true;	// phone
	return false;
}

function check_product_fields($params) {
	if (check_number($params['shop']) &&
		check_string($params['name']) &&
		check_number($params['count']) &&
		check_number($params['price'])) return true;
	return false;
}



/* ============================================================ */
/* database */
/* ============================================================ */

function db_create_tables() {
	$db = new SQLite3(DATABASE_FILENAME);

	$db->exec('CREATE TABLE IF NOT EXISTS shops (id INTEGER PRIMARY KEY, name, address, director_name, phone)');
	$db->exec('CREATE TABLE IF NOT EXISTS stuff (shop, name, position, year, address, phone)');
	$db->exec('CREATE TABLE IF NOT EXISTS products (shop, name, count, price)');
}

function db_add_shop($params) {
	$db = new SQLite3(DATABASE_FILENAME);

	$stmt = $db->prepare('INSERT INTO shops (name, address, director_name, phone) VALUES (:name, :address, :director_name, :phone)');
	$stmt->bindValue(':name',          $params['name']);
	$stmt->bindValue(':address',       $params['address']);
	$stmt->bindValue(':director_name', $params['director-name']);
	$stmt->bindValue(':phone',         $params['phone']);

	$stmt->execute();

	return true;
}

function db_delete_shop($index) {
	$db = new SQLite3(DATABASE_FILENAME);

	$stmt = $db->prepare('DELETE FROM shops WHERE id=:index');
	$stmt->bindValue(':index', $index);

	$stmt->execute();

	return true;
}

function db_add_stuff($params) {
	$db = new SQLite3(DATABASE_FILENAME);

	$stmt = $db->prepare('INSERT INTO stuff (shop, name, position, year, address, phone) VALUES (:shop, :name, :position, :year, :address, :phone)');
	$stmt->bindValue(':shop',     $params[0]);
	$stmt->bindValue(':name',     $params[1]);
	$stmt->bindValue(':position', $params[2]);
	$stmt->bindValue(':year',     $params[3]);
	$stmt->bindValue(':address',  $params[4]);
	$stmt->bindValue(':phone',    $params[5]);

	$stmt->execute();

	return true;
}

function db_add_product($params) {
	$db = new SQLite3(DATABASE_FILENAME);

	$stmt = $db->prepare('INSERT INTO products (shop, name, count, price) VALUES (:shop, :name, :count, :price)');
	$stmt->bindValue(':shop',  $params['shop']);
	$stmt->bindValue(':name',  $params['name']);
	$stmt->bindValue(':count', $params['count']);
	$stmt->bindValue(':price', $params['price']);

	$stmt->execute();

	return true;
}

function db_get_shops($only_id_and_name = false) {
	$db = new SQLite3(DATABASE_FILENAME);
	if ($only_id_and_name) {
		$sql = 'SELECT id, name FROM shops'; 
	} else {
		$sql = 'SELECT name, address, director_name, phone, id FROM shops';
	}
	$r = $db->query($sql);
	$res = array();
	$i = 0;
	while($row = $r->fetchArray(SQLITE3_ASSOC)) {
		if (!$only_id_and_name) $row['id'] = '<a href="shops.php?delete=' . $row['id'] . '">удалить</a>';
		$res[$i] = $row;
		$i++;
	} 
	return $res;
}

function db_get_stuff() {
	$db = new SQLite3(DATABASE_FILENAME);
	$r = $db->query(
		'SELECT shops.name, stuff.name, stuff.position, stuff.year, stuff.address, stuff.phone '.
		'FROM stuff '.
		'LEFT JOIN shops ON stuff.shop=shops.id');
	$res = array();
	$i = 0;
	while($row = $r->fetchArray(SQLITE3_NUM)) { 
		$res[$i] = $row;
		$i++;
	} 
	return $res;
}

function db_get_products() {
	$db = new SQLite3(DATABASE_FILENAME);
	$r = $db->query(
		'SELECT shops.name, products.name, products.count, products.price '.
		'FROM products '.
		'LEFT JOIN shops ON products.shop=shops.id');
	$res = array();
	$i = 0;
	while($row = $r->fetchArray(SQLITE3_NUM)) { 
		$res[$i] = $row;
		$i++;
	}
	return $res;
}



/* ============================================================ */
/* render table */
/* ============================================================ */

$title_shop = array(
	'Название магазина', 
	'Адрес магазина', 
	'Фамилия заведущего', 
	'Телефон', 
	'Удалить'
	);

$title_stuff = array(
	'Название магазина',
	'Фамилия',
	'Должность',
	'Год рождения',
	'Домашний адрес',
	'Телефон'
	);

$title_product = array(
	'Название магазина',
	'Наименование товара',
	'Количество',
	'Цена за единицу',
	);

function render_table($title, $body) {
	echo '<table class="table table-bordered">';
	
	echo '<thead></tr>';
	foreach ($title as $value) {
		echo '<th>' . $value . '</th>';
	}
	echo '</tr></thead>';

	echo '<tbody></tr>';
	foreach ($body as $row) {
		echo '<tr>';
		foreach ($row as $value) {
			echo '<td>' . $value . '</td>';
		}
		echo '</tr>';
	}
	echo '</tr></tbody>';

	echo '</table>';
}




db_create_tables();

?>