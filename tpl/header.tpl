<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Магазины</title>
	<link rel="stylesheet" href="css/bootstrap.min.css">
	<link rel="stylesheet" href="css/sweetalert.css">
</head>
<body>
	<nav class="navbar navbar-default">
		<div class="container">
			<!-- Brand and toggle get grouped for better mobile display -->
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="shops.php">Магазины</a>
			</div>

			<!-- Collect the nav links, forms, and other content for toggling -->
			<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
				<ul class="nav navbar-nav">
					<li<?php if ($page == 'shops')    echo ' class="active"'; ?>><a href="shops.php">Добавить</a></li>
					<li<?php if ($page == 'stuff')    echo ' class="active"'; ?>><a href="stuff.php">Персонал</a></li>
					<li<?php if ($page == 'products') echo ' class="active"'; ?>><a href="products.php">Товары</a></li>
				</ul>
			</div>
		</div>
	</nav>

