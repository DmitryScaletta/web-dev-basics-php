<?php
	require_once('_functions.php');

	$page = 'shops';
	require_once('tpl/header.tpl');

	if ($_SERVER['REQUEST_METHOD'] === 'POST') {
		$params['name']          = $_POST['shop-name']          ?? "";
		$params['address']       = $_POST['shop-address']       ?? "";
		$params['director-name'] = $_POST['shop-director-name'] ?? "";
		$params['phone']         = $_POST['shop-phone']         ?? "";
		
		if ($_POST['shop-checkbox'] ?? false && 
			check_shop_fields($params['name'], $params['address'], $params['director-name'], $params['phone']))
		{
			db_add_shop($params);
		}
	}
	if ($_SERVER['REQUEST_METHOD'] === 'GET') {
		$id = $_GET['delete'] ?? false;
		if ($id) {
			db_delete_shop($id);
			header('Location: ' . $_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['HTTP_HOST'] . $_SERVER['SCRIPT_NAME']);
		}
	}

	// var_dump($_SERVER);

	$table_data = db_get_shops();
?>
	<div class="container">
		<h2>Добавление магазина</h2>
		<form action="shops.php" method="POST" id="form-add-shop">
			<div class="row">
				<div class="col-md-4">
					<div class="form-group">
						<label class="control-label" for="shop-name">Название магазина *</label>
						<input type="text" class="form-control" id="shop-name" name="shop-name" placeholder="Название магазина">
					</div>
					<div class="form-group">
						<label class="control-label" for="shop-address">Адрес магазина *</label>
						<input type="text" class="form-control" id="shop-address" name="shop-address" placeholder="Адрес магазина">
					</div>
				</div>
				<div class="col-md-4">
					<div class="form-group">
						<label class="control-label" for="shop-director-name">Фамилия заведущего</label>
						<input type="text" class="form-control" id="shop-director-name" name="shop-director-name" placeholder="Фамилия заведущего">
					</div>
					<div class="form-group">
						<label class="control-label" for="shop-phone">Телефон *</label>
						<input type="text" class="form-control" id="shop-phone" name="shop-phone" placeholder="+375XXXXXXXXX">
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-8">
					<div class="checkbox">
						<label>
							<input type="checkbox" value="1" name="shop-checkbox">Я действительно хочу добавить магазин
						</label>
					</div>
					<button type="submit" class="btn btn-primary" id="add-shop">Добавить</button>
					<button type="reset" class="btn btn-default">Очистить все поля</button>
				</div>
			</div>
		</form>

		<div class="row">
			<div class="col-md-12">
				<h2>Список магазинов</h2>
				<?php render_table($title_shop, $table_data); ?>
			</div>
		</div>
	</div>
<?php require_once('tpl/footer.tpl'); ?>