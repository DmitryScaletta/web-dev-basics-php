$(document).ready(function() {
	
	/* ============================================================ */
	/* checking */
	/* ============================================================ */

	var PHONE_REGEX  = /^\+[\d]{12}$/;

	var COMPULSORY_FIELDS_MESSAGE       = "Пожалуйста, заполните все обязательные поля!";
	var COMPULSORY_FIELDS_MESSAGE_TITLE = "Внимание"


	function check_string(s) {
		if (s.length < 1) return false;
		return true;
	}

	function check_phone(s) {
		if (!PHONE_REGEX.exec(s)) return false;
		return true;
	}

	function check_year(s) {
		if (s < 1900 || s > new Date().getFullYear()) return false;
		return true;
	}

	function check_number(s) {
		if (isNaN(s) || s < 1) return false;
		return true;
	}

	function check_field(e) {
		is_error = true;
		switch (e.data.type) {
			case "string": is_error = check_string(this.value); break;
			case "number": is_error = check_number(this.value); break;
			case "phone":  is_error = check_phone(this.value);  break;
			case "year":   is_error = check_year(this.value);   break;
		}

		if (is_error) {
			$(this).parent(".form-group").removeClass("has-error").addClass("has-success");
		} else {
			if (e.data.compulsory) 	$(this).parent(".form-group").removeClass("has-success").addClass("has-error");
			else					$(this).parent(".form-group").removeClass("has-success").removeClass("has-error");
		}
	}

	function check_all_fields_shop() {
		$("#shop-name")   .blur();
		$("#shop-address").blur();
		$("#shop-phone")  .blur();
	}

	function check_all_fields_stuff() {
		$("#stuff-shop")    .blur();
		$("#stuff-name")    .blur();
		$("#stuff-position").blur();
		$("#stuff-year")    .blur();
		$("#stuff-phone")   .blur();
	}

	function check_all_fields_product() {
		$("#product-shop") .blur()
		$("#product-name") .blur()
		$("#product-price").blur()
	}

	function only_numbers(e) {
		if ((e.keyCode >= 48 && e.keyCode <= 57) || (e.keyCode >= 96 && e.keyCode <= 105) || e.keyCode == 8 || e.keyCode == 46) { 
			return true;
		} else {
			return false;
		}
	}



	/* ============================================================ */
	/* set focus */
	/* ============================================================ */

	$("#shop-name").focus();
	$("#stuff-shop").focus();
	$("#product-shop").focus();



	/* ============================================================ */
	/* events */
	/* ============================================================ */

	$("#stuff-year")   .keydown(only_numbers);
	$("#product-price").keydown(only_numbers);

	$("#shop-name")         .on("blur keypress", { type: "string", compulsory: true  }, check_field);
	$("#shop-address")      .on("blur keypress", { type: "string", compulsory: true  }, check_field);
	$("#shop-director-name").on("blur keypress", { type: "string", compulsory: false }, check_field);
	$("#shop-phone")        .on("blur keypress", { type: "phone",  compulsory: true  }, check_field);

	$("#stuff-shop")        .on("blur keypress", { type: "number", compulsory: true  }, check_field);
	$("#stuff-name")        .on("blur keypress", { type: "string", compulsory: true  }, check_field);
	$("#stuff-position")    .on("blur keypress", { type: "string", compulsory: true  }, check_field);
	$("#stuff-year")        .on("blur keypress", { type: "year",   compulsory: true  }, check_field);
	$("#stuff-address")     .on("blur keypress", { type: "string", compulsory: false }, check_field);
	$("#stuff-phone")       .on("blur keypress", { type: "phone",  compulsory: true  }, check_field);

	$("#product-shop")      .on("blur keypress", { type: "number", compulsory: true  }, check_field);
	$("#product-name")      .on("blur keypress", { type: "string", compulsory: true  }, check_field);
	$("#product-price")     .on("blur keypress", { type: "number", compulsory: true  }, check_field);	

	$("button[type=reset]").on("click", function() {
		check_all_fields_shop();
		check_all_fields_stuff();
		check_all_fields_product();
	});

	$("#form-add-shop").submit(function() {
		if (!check_string($("#shop-name").val()) || 
			!check_string($("#shop-address").val()) || 
			!check_phone( $("#shop-phone").val())) 
		{
			sweetAlert(COMPULSORY_FIELDS_MESSAGE_TITLE, COMPULSORY_FIELDS_MESSAGE, "error");
			check_all_fields_shop();
			return false;
		}
	});
	$("#form-add-stuff").on("submit", function() {
		if (!check_number($("#stuff-shop").val()) || 
			!check_string($("#stuff-name").val()) || 
			!check_string($("#stuff-position").val()) || 
			!check_year(  $("#stuff-year").val()) || 
			!check_phone( $("#stuff-phone").val())) 
		{
			sweetAlert(COMPULSORY_FIELDS_MESSAGE_TITLE, COMPULSORY_FIELDS_MESSAGE, "error");
			check_all_fields_stuff();
			return false;
		}
	});
	$("#form-add-product").on("submit", function() {
		if (!check_number($("#product-shop").val()) || 
			!check_string($("#product-name").val()) || 
			!check_number($("#product-price").val())) 
		{
			sweetAlert(COMPULSORY_FIELDS_MESSAGE_TITLE, COMPULSORY_FIELDS_MESSAGE, "error");
			check_all_fields_product();
			return false;
		}
	});

});