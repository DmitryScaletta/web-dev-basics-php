<?php
	require_once('_functions.php');

	$page = 'products';
	require_once('tpl/header.tpl');

	if ($_SERVER['REQUEST_METHOD'] === 'POST') {
		$params['shop']  = $_POST['product-shop']  ?? "";
		$params['name']  = $_POST['product-name']  ?? "";
		$params['count'] = $_POST['product-count'] ?? "";
		$params['price'] = $_POST['product-price'] ?? "";

		if (check_product_fields($params))
		{
			db_add_product($params);
		}
	}

	$table_data = db_get_products();

	$shops = db_get_shops(true);

?>
	<div class="container">
		<h2>Добавление товара</h2>
		<form action="products.php" method="POST" id="form-add-product">
			<div class="row">
				<div class="col-md-4">
					<div class="form-group">
						<label class="control-label">Название магазина *</label>
						<select class="form-control" id="product-shop" name="product-shop">
							<?php
								foreach ($shops as $shop) {
									echo '<option value="' . $shop['id'] . '">' . $shop['name'] . '</option>';
								}
							?>
						</select>
					</div>
					<div class="form-group">
						<label class="control-label" for="product-name">Наименование товара *</label>
						<input type="text" class="form-control" id="product-name" name="product-name" placeholder="Наименование товара">
					</div>
				</div>
				<div class="col-md-4">
					<div class="form-group">
						<label class="control-label" for="product-price">Цена за единицу *</label>
						<input type="text" class="form-control" id="product-price" name="product-price" placeholder="Цена за единицу">
					</div>
					<div class="form-group">
						<label class="control-label">Колличество *</label>
						<div>
							<label class="radio-inline">
								<input type="radio" name="product-count" value="1" checked> 1
							</label>
							<label class="radio-inline">
								<input type="radio" name="product-count" value="2"> 2
							</label>
							<label class="radio-inline">
								<input type="radio" name="product-count" value="3"> 3
							</label>
							<label class="radio-inline">
								<input type="radio" name="product-count" value="4"> 4
							</label>
							<label class="radio-inline">
								<input type="radio" name="product-count" value="5"> 5
							</label>
							<label class="radio-inline">
								<input type="radio" name="product-count" value="6"> 6
							</label>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-8">
					<button type="submit" class="btn btn-primary" id="add-product">Добавить</button>
					<button type="reset" class="btn btn-default">Очистить все поля</button>
				</div>
			</div>
		</form>

		<div class="row">
			<div class="col-md-12">
				<h2>Список товаров</h2>
				<?php render_table($title_product, $table_data); ?>
			</div>
		</div>
	</div>
<?php require_once('tpl/footer.tpl'); ?>